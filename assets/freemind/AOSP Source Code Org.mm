<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1351534037636" ID="ID_1005163662" MODIFIED="1351534067597" TEXT="AOSP Source Code Org">
<node CREATED="1351534098597" ID="ID_983085463" MODIFIED="1351534114657" POSITION="right" TEXT="abi">
<node CREATED="1351534129987" ID="ID_1243449163" MODIFIED="1351534134157" TEXT="cpp">
<node CREATED="1351534146031" ID="ID_890263437" MODIFIED="1351534151176" TEXT="include"/>
<node CREATED="1351534177365" ID="ID_742186856" MODIFIED="1351534180572" TEXT="src"/>
</node>
</node>
<node CREATED="1351534196299" ID="ID_1082312367" MODIFIED="1351534201509" POSITION="right" TEXT="bionic">
<node CREATED="1351534212622" ID="ID_280935770" MODIFIED="1351534217144" TEXT="libc"/>
<node CREATED="1351534241603" ID="ID_214022273" MODIFIED="1351534245801" TEXT="libdl"/>
<node CREATED="1351534255843" ID="ID_176387075" MODIFIED="1351534262211" TEXT="libm"/>
<node CREATED="1351534274418" ID="ID_191339000" MODIFIED="1351534281584" TEXT="libstdc++"/>
<node CREATED="1351534293163" ID="ID_537005123" MODIFIED="1351534301150" TEXT="libthread_db"/>
<node CREATED="1351534312902" ID="ID_944532217" MODIFIED="1351534317501" TEXT="linker"/>
</node>
<node CREATED="1351534331465" ID="ID_1956963197" MODIFIED="1351534336652" POSITION="right" TEXT="bootable">
<node CREATED="1351534343198" ID="ID_984007430" MODIFIED="1351534347573" TEXT="bootloader"/>
<node CREATED="1351534357418" ID="ID_204858728" MODIFIED="1351534362511" TEXT="diskinstaller"/>
<node CREATED="1351534372310" ID="ID_707466933" MODIFIED="1351534376212" TEXT="recovery"/>
</node>
<node CREATED="1351534391019" ID="ID_267537299" MODIFIED="1351534396358" POSITION="right" TEXT="build"/>
<node CREATED="1351534428203" ID="ID_1265387903" MODIFIED="1351534431960" POSITION="right" TEXT="cts"/>
<node CREATED="1351534439763" ID="ID_502210497" MODIFIED="1351534443750" POSITION="right" TEXT="dalvik"/>
<node CREATED="1351534451473" ID="ID_922439265" MODIFIED="1351534456512" POSITION="right" TEXT="development">
<node CREATED="1351534486612" ID="ID_365850295" MODIFIED="1351534490510" TEXT="ide">
<node CREATED="1351534495629" ID="ID_492335371" MODIFIED="1351534499264" TEXT="eclipse"/>
</node>
</node>
<node CREATED="1351534563439" ID="ID_1964530335" MODIFIED="1351534568480" POSITION="right" TEXT="device"/>
<node CREATED="1351534579537" ID="ID_1341883460" MODIFIED="1351534583399" POSITION="left" TEXT="docs"/>
<node CREATED="1351534632331" ID="ID_1712338667" MODIFIED="1351534637060" POSITION="left" TEXT="external"/>
<node CREATED="1351534687703" ID="ID_1865968191" MODIFIED="1351534691236" POSITION="left" TEXT="system"/>
<node CREATED="1351534698448" ID="ID_1786485010" MODIFIED="1351534701376" POSITION="left" TEXT="sdk"/>
<node CREATED="1351534708986" ID="ID_1211754976" MODIFIED="1351534716047" POSITION="left" TEXT="prebuilts"/>
<node CREATED="1351534725828" ID="ID_1727073303" MODIFIED="1351534731891" POSITION="left" TEXT="prebuilt"/>
<node CREATED="1351534741459" ID="ID_1261582237" MODIFIED="1351534745354" POSITION="left" TEXT="packages"/>
<node CREATED="1351534833457" ID="ID_587790730" MODIFIED="1351534836694" POSITION="left" TEXT="ndk"/>
<node CREATED="1351534844532" ID="ID_92924989" MODIFIED="1351534848159" POSITION="left" TEXT="libcore"/>
<node CREATED="1351534860098" ID="ID_299725940" MODIFIED="1351534865180" POSITION="left" TEXT="hardware"/>
<node CREATED="1351534881527" ID="ID_1818535941" MODIFIED="1351534886411" POSITION="left" TEXT="frameworks">
<node CREATED="1351534893928" ID="ID_1319600935" MODIFIED="1351535526766" TEXT="base">
<node CREATED="1351535040898" ID="ID_926274828" MODIFIED="1351535044259" TEXT="api"/>
<node CREATED="1351535054139" ID="ID_1222276754" MODIFIED="1351535057673" TEXT="build"/>
<node CREATED="1351535067138" ID="ID_1238998153" MODIFIED="1351535070399" TEXT="cmds"/>
<node CREATED="1351535079907" ID="ID_1298806891" MODIFIED="1351539503007" TEXT="core">
<node CREATED="1351535907961" ID="ID_1447901668" MODIFIED="1351539504892" TEXT="java">
<node CREATED="1351536895214" FOLDED="true" ID="ID_813686934" MODIFIED="1351537037718" TEXT="android">
<node CREATED="1351535956411" ID="ID_670101967" MODIFIED="1351535972384" TEXT="accessibilityservice"/>
<node CREATED="1351536031369" ID="ID_1175952548" MODIFIED="1351536037034" TEXT="accounts"/>
<node CREATED="1351536373717" ID="ID_8768396" MODIFIED="1351536380733" TEXT="animation"/>
<node CREATED="1351536392362" ID="ID_1397665897" MODIFIED="1351536399023" TEXT="annotation"/>
<node CREATED="1351536410648" ID="ID_737462009" MODIFIED="1351536415327" TEXT="app"/>
<node CREATED="1351536425619" ID="ID_1117788890" MODIFIED="1351536435822" TEXT="appwidget"/>
<node CREATED="1351536445061" ID="ID_1116248605" MODIFIED="1351536451303" TEXT="bluetooth"/>
<node CREATED="1351536460140" ID="ID_1999183991" MODIFIED="1351536465681" TEXT="content"/>
<node CREATED="1351536473818" ID="ID_1307750886" MODIFIED="1351536480420" TEXT="database"/>
<node CREATED="1351536491321" ID="ID_166978406" MODIFIED="1351536495048" TEXT="ddm"/>
<node CREATED="1351536503619" ID="ID_717010288" MODIFIED="1351536510319" TEXT="debug"/>
<node CREATED="1351536519909" ID="ID_1797636190" MODIFIED="1351536524577" TEXT="emoji"/>
<node CREATED="1351536533581" ID="ID_719196664" MODIFIED="1351536538925" TEXT="gesture"/>
<node CREATED="1351536549363" ID="ID_1150212703" MODIFIED="1351536555168" TEXT="hardware"/>
<node CREATED="1351536566630" ID="ID_644451938" MODIFIED="1351536581140" TEXT="inputmethodservice"/>
<node CREATED="1351536591358" ID="ID_950417078" MODIFIED="1351536596070" TEXT="net"/>
<node CREATED="1351536600982" ID="ID_305629781" MODIFIED="1351536605064" TEXT="nfc"/>
<node CREATED="1351536615603" ID="ID_1687141512" MODIFIED="1351536618831" TEXT="os"/>
<node CREATED="1351536631118" ID="ID_1808060768" MODIFIED="1351536634016" TEXT="pim"/>
<node CREATED="1351536664537" ID="ID_552047421" MODIFIED="1351536671343" TEXT="preference"/>
<node CREATED="1351536696992" ID="ID_94848920" MODIFIED="1351536703794" TEXT="provider"/>
<node CREATED="1351536717420" ID="ID_832387636" MODIFIED="1351536726058" TEXT="server"/>
<node CREATED="1351536736002" ID="ID_246474555" MODIFIED="1351536748059" TEXT="service"/>
<node CREATED="1351536758678" ID="ID_486713489" MODIFIED="1351536763429" TEXT="speech"/>
<node CREATED="1351536781862" ID="ID_999798918" MODIFIED="1351536788089" TEXT="test"/>
<node CREATED="1351536799901" ID="ID_26924889" MODIFIED="1351536804553" TEXT="text"/>
<node CREATED="1351536814547" ID="ID_1488477881" MODIFIED="1351536818612" TEXT="util"/>
<node CREATED="1351536828779" ID="ID_153358228" MODIFIED="1351536832560" TEXT="view"/>
<node CREATED="1351536845699" ID="ID_1067112187" MODIFIED="1351536849708" TEXT="webkit"/>
<node CREATED="1351536862981" ID="ID_542988268" MODIFIED="1351536866977" TEXT="widget"/>
</node>
<node CREATED="1351537054488" ID="ID_905346995" MODIFIED="1351537058967" TEXT="com">
<node CREATED="1351537089057" ID="ID_965273439" MODIFIED="1351537094452" TEXT="android">
<node CREATED="1351537100227" ID="ID_1724971115" MODIFIED="1351537104953" TEXT="internal">
<node CREATED="1351537125155" ID="ID_272977366" MODIFIED="1351537128310" TEXT="app"/>
<node CREATED="1351537140464" ID="ID_1923507975" MODIFIED="1351537145329" TEXT="appwidget"/>
<node CREATED="1351537155999" ID="ID_50403876" MODIFIED="1351537164254" TEXT="backup"/>
<node CREATED="1351537222603" ID="ID_692362028" MODIFIED="1351537227420" TEXT="content"/>
<node CREATED="1351537239003" ID="ID_1781643183" MODIFIED="1351537252182" TEXT="database"/>
<node CREATED="1351537260697" ID="ID_1614591223" MODIFIED="1351537264469" TEXT="http"/>
<node CREATED="1351537273869" ID="ID_7823038" MODIFIED="1351537278980" TEXT="logging"/>
<node CREATED="1351537312450" ID="ID_1481717967" MODIFIED="1351537316014" TEXT="net"/>
<node CREATED="1351537322960" ID="ID_520268821" MODIFIED="1351537325874" TEXT="os"/>
<node CREATED="1351537333286" ID="ID_1303407867" MODIFIED="1351537337559" TEXT="policy"/>
<node CREATED="1351537346027" ID="ID_627190850" MODIFIED="1351537351093" TEXT="preference"/>
<node CREATED="1351537361483" ID="ID_1686808018" MODIFIED="1351537366268" TEXT="statusbar"/>
<node CREATED="1351537377970" ID="ID_6783200" MODIFIED="1351537385042" TEXT="textservice"/>
<node CREATED="1351537393167" ID="ID_1105159035" MODIFIED="1351537396591" TEXT="util"/>
<node CREATED="1351537405343" ID="ID_1227749786" MODIFIED="1351537408465" TEXT="view"/>
<node CREATED="1351537419311" ID="ID_1235607363" MODIFIED="1351537422711" TEXT="widget"/>
</node>
<node CREATED="1351537442892" ID="ID_156231579" MODIFIED="1351537447820" TEXT="server"/>
</node>
<node CREATED="1351537473706" ID="ID_1157480291" MODIFIED="1351537477972" TEXT="google">
<node CREATED="1351537492852" ID="ID_1863463056" MODIFIED="1351537496633" TEXT="collect"/>
<node CREATED="1351537512400" ID="ID_280212459" MODIFIED="1351537515726" TEXT="mms"/>
<node CREATED="1351538652996" ID="ID_650000691" MODIFIED="1351538652996" TEXT=""/>
<node CREATED="1351537525943" ID="ID_1354315390" MODIFIED="1351537529725" TEXT="util"/>
</node>
</node>
</node>
</node>
<node CREATED="1351535093590" ID="ID_868541709" MODIFIED="1351535096819" TEXT="data"/>
<node CREATED="1351535105536" ID="ID_1140482725" MODIFIED="1351535109755" TEXT="docs"/>
<node CREATED="1351535120377" ID="ID_356215425" MODIFIED="1351539514411" TEXT="drm">
<node CREATED="1351538097821" ID="ID_435248554" MODIFIED="1351539516541" TEXT="java">
<node CREATED="1351538114038" ID="ID_1356295999" MODIFIED="1351538165930" TEXT="android">
<node CREATED="1351538172529" ID="ID_1224117153" MODIFIED="1351538177613" TEXT="drm"/>
</node>
</node>
</node>
<node CREATED="1351535132273" ID="ID_310913656" MODIFIED="1351539515540" TEXT="graphics">
<node CREATED="1351537569852" ID="ID_85687651" MODIFIED="1351539517394" TEXT="java">
<node CREATED="1351537978153" ID="ID_88450170" MODIFIED="1351537986759" TEXT="android">
<node CREATED="1351537998698" ID="ID_732978842" MODIFIED="1351538011992" TEXT="graphics"/>
<node CREATED="1351538028759" ID="ID_361482816" MODIFIED="1351538038517" TEXT="renderscript"/>
</node>
</node>
</node>
<node CREATED="1351535144661" ID="ID_1754754031" MODIFIED="1351539519445" TEXT="icu4j">
<node CREATED="1351538213312" ID="ID_1796452164" MODIFIED="1351539520517" TEXT="java">
<node CREATED="1351538224419" ID="ID_719404888" MODIFIED="1351538231011" TEXT="android">
<node CREATED="1351538244315" ID="ID_1559073431" MODIFIED="1351538248979" TEXT="icu"/>
</node>
</node>
</node>
<node CREATED="1351535156181" ID="ID_358236044" MODIFIED="1351535161794" TEXT="include"/>
<node CREATED="1351535170677" ID="ID_1021163300" MODIFIED="1351539522431" TEXT="keystore">
<node CREATED="1351538274278" ID="ID_1619563696" MODIFIED="1351539523385" TEXT="java">
<node CREATED="1351538590465" ID="ID_984408884" MODIFIED="1351538596122" TEXT="android">
<node CREATED="1351538601512" ID="ID_1530961193" MODIFIED="1351538606584" TEXT="security"/>
</node>
</node>
</node>
<node CREATED="1351535194589" ID="ID_1640174033" MODIFIED="1351535198030" TEXT="libs"/>
<node CREATED="1351535206992" ID="ID_947771810" MODIFIED="1351539530649" TEXT="location">
<node CREATED="1351538636969" FOLDED="true" ID="ID_1520816480" MODIFIED="1351539327797" TEXT="java">
<node CREATED="1351538676629" ID="ID_283712374" MODIFIED="1351538681995" TEXT="android">
<node CREATED="1351538695255" ID="ID_1407009583" MODIFIED="1351538699701" TEXT="location"/>
</node>
<node CREATED="1351538710507" ID="ID_1064238148" MODIFIED="1351538714969" TEXT="com">
<node CREATED="1351538730002" ID="ID_1669645263" MODIFIED="1351538733773" TEXT="android">
<node CREATED="1351538738032" ID="ID_238806347" MODIFIED="1351538742181" TEXT="internal">
<node CREATED="1351538758251" ID="ID_1254863930" MODIFIED="1351538762781" TEXT="location"/>
</node>
<node CREATED="1351538787937" ID="ID_1011714070" MODIFIED="1351538787937" TEXT=""/>
</node>
</node>
</node>
</node>
<node CREATED="1351535219149" ID="ID_167059101" MODIFIED="1351539529655" TEXT="media">
<node CREATED="1351538839684" ID="ID_1803553299" MODIFIED="1351538847254" TEXT="java">
<node CREATED="1351538867783" ID="ID_381093672" MODIFIED="1351538872118" TEXT="android">
<node CREATED="1351538888356" ID="ID_772855184" MODIFIED="1351538892184" TEXT="drm"/>
<node CREATED="1351538904694" ID="ID_454050980" MODIFIED="1351538910631" TEXT="media"/>
<node CREATED="1351538917109" ID="ID_1568642813" MODIFIED="1351538920819" TEXT="mtp"/>
</node>
</node>
</node>
<node CREATED="1351535231177" ID="ID_839724663" MODIFIED="1351535234655" TEXT="native"/>
<node CREATED="1351535253579" ID="ID_925482384" MODIFIED="1351539532031" TEXT="nfc-extras">
<node CREATED="1351538946384" ID="ID_1712696176" MODIFIED="1351538950289" TEXT="java">
<node CREATED="1351538961145" ID="ID_564224797" MODIFIED="1351538964957" TEXT="com">
<node CREATED="1351538979650" ID="ID_677586750" MODIFIED="1351538984536" TEXT="android">
<node CREATED="1351539001997" ID="ID_585137393" MODIFIED="1351539008548" TEXT="nfc_extras"/>
</node>
</node>
</node>
</node>
<node CREATED="1351535268494" ID="ID_1722643700" MODIFIED="1351539532785" TEXT="obex">
<node CREATED="1351539031785" ID="ID_52047994" MODIFIED="1351539035987" TEXT="javax">
<node CREATED="1351539048759" ID="ID_1551138742" MODIFIED="1351539057454" TEXT="obex"/>
</node>
</node>
<node CREATED="1351535279974" ID="ID_1309549502" MODIFIED="1351539533577" TEXT="opengl">
<node CREATED="1351539077892" ID="ID_992608965" MODIFIED="1351539081827" TEXT="java">
<node CREATED="1351539095834" ID="ID_1452086470" MODIFIED="1351539101252" TEXT="android">
<node CREATED="1351539119111" ID="ID_548712250" MODIFIED="1351539123970" TEXT="opengl"/>
</node>
<node CREATED="1351539140890" ID="ID_866109306" MODIFIED="1351539144631" TEXT="com">
<node CREATED="1351539146983" ID="ID_374368107" MODIFIED="1351539150792" TEXT="google">
<node CREATED="1351539177437" ID="ID_955800571" MODIFIED="1351539182495" TEXT="android">
<node CREATED="1351539205848" ID="ID_146997902" MODIFIED="1351539212888" TEXT="gles_jni"/>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1351535293017" ID="ID_67789220" MODIFIED="1351535298500" TEXT="packages"/>
<node CREATED="1351535306925" ID="ID_1992562265" MODIFIED="1351535310951" TEXT="policy"/>
<node CREATED="1351535319382" ID="ID_1955358724" MODIFIED="1351539536336" TEXT="sax">
<node CREATED="1351539268803" ID="ID_1972732940" MODIFIED="1351539278167" TEXT="java">
<node CREATED="1351539285163" ID="ID_442221575" MODIFIED="1351539289419" TEXT="android">
<node CREATED="1351539300558" ID="ID_473807143" MODIFIED="1351539309884" TEXT="sax"/>
</node>
</node>
</node>
<node CREATED="1351535331044" ID="ID_490931554" MODIFIED="1351535335192" TEXT="services"/>
<node CREATED="1351535346316" ID="ID_446261680" MODIFIED="1351535351790" TEXT="telephony"/>
<node CREATED="1351535366752" ID="ID_1001489892" MODIFIED="1351535370911" TEXT="testrunner"/>
<node CREATED="1351535391240" ID="ID_883889580" MODIFIED="1351535396249" TEXT="tests"/>
<node CREATED="1351535407315" ID="ID_1586999730" MODIFIED="1351535480816" TEXT="tools"/>
<node CREATED="1351535499485" ID="ID_1624850386" MODIFIED="1351535503986" TEXT="voip"/>
<node CREATED="1351535513403" ID="ID_280660808" MODIFIED="1351535517032" TEXT="wifi"/>
</node>
</node>
</node>
</map>
